import 'babel-polyfill';
import './modernizr';

// 전역변수 설정
const WIN = window;
const DOC = document;

DOC.addEventListener('DOMContentLoaded', () => {
  WIN.resizeTo(462, WIN.outerHeight - WIN.innerHeight + DOC.body.offsetHeight); // 브라우저별 UI에 맞는 크기로 윈도우 리사이징

  if (PRIVATE_CHAT.dateMode === 'ACT_DATE') {
    if (Modernizr.websockets) {
      connectSocket(); // 채팅 수락자가 웹소켓을 사용 가능한 경우에만 연결 시도
    } else {
      alert(
        '채팅 실시간 통신을 위한 기술을 지원하지 않는 브라우저입니다.\n최신 브라우저를 이용해주세요.'
      );
    }
  } else if (PRIVATE_CHAT.dateMode === 'REQ_DATE') {
    DOC.getElementById('approve-button--ok').focus();
  }

  // 이벤트 리스너 및 이벤트 패키지 설정
  DOC.addEventListener('click', event => {
    const findJSClassName = Array.from(event.target.classList).find(item => item.startsWith('js-'));
    const eventPack = (() => {
      return {
        'js-submit-chat': () => {
          sendMessage(event);
        },
        'js-complaint': () => {
          DOC.getElementById('complaint-layer').classList.toggle('complaint-layer--activated');
        },
        'js-complaint-submit--ok': () => {
          const complaintLayer = DOC.getElementById('complaint-layer');

          if (complaintLayer.querySelector('#complaint-content').value.trim().length < 5) {
            alert('신고내용을 5글자 이상 작성해주세요.');
            return false;
          }

          promiseAjax('POST', './ajax.common.php', {
            cmd: 'report_chat_single',
            target_mb_id: PRIVATE_CHAT.targetUserId,
            category: 'chat_single',
            kind: complaintLayer.querySelector(':checked').value,
            content: complaintLayer.querySelector('#complaint-content').value.trim()
          })
            .then(result => {
              const data = JSON.parse(result.responseText);

              if (data.returnCode === 'SUCCESS') {
                alert('신고가 접수되었습니다.');
              } else {
                if (data.errorMsg.includes('targetUserId')) {
                  // 존재하지 않는 targetuserld 입니다. 에러 메세지 내용을 일반 사용자가 이해할수 있도록 출력함
                  alert('손님은 신고할 수 없습니다.');
                } else {
                  if (data.errorMsg.includes('content은')) {
                    // content은 5자 이상이어야 합니다. 에러 메세지 내용을 일반 사용자가 이해할수 있도록 출력함
                    alert('신고내용을 5글자 이상 작성해주세요.');
                  } else {
                    alert(data.errorMsg);
                  }
                }
              }

              document
                .getElementById('complaint-layer')
                .classList.remove('complaint-layer--activated');
              document.getElementById('complaint-content').value = '';
            })
            .catch(error => {
              alert(error);
            });
        },
        'js-approve-button--ok': () => {
          // 수락자는 대기 페이지가 없기 때문에 버튼을 제공하지 않으므로 신청자에게만 유효함
          if (!event.target.classList.contains('button--activated')) {
            return false;
          }

          promiseAjax('GET', './date_consume_ajax.php', location.search)
            .then(result => {
              try {
                // 포인트 선 차감 로직을 무사히 돌렸다면 반환받은 데이터를 초기화함
                const parsedResponseText = JSON.parse(result.responseText);

                PRIVATE_CHAT.userInfo = parsedResponseText.userInfo;
                PRIVATE_CHAT.sessionId = parsedResponseText.sessionId;
                PRIVATE_CHAT.key = parsedResponseText.key;
                PRIVATE_CHAT.connectMode = parsedResponseText.connectMode;

                // 채팅 신청자는 소켓 연결과 동시에 먼저 채팅방에 입장
                if (Modernizr.websockets) {
                  connectSocket();
                  switchView('chat', true); // 채팅 신청 대기화면을 채팅 화면으로 전환시켜줌
                } else {
                  alert(
                    '채팅 실시간 통신을 위한 기술을 지원하지 않는 브라우저입니다.\n최신 브라우저를 이용해주세요.'
                  );
                }
              } catch (error) {
                // JSON 파싱에 실패하면 catch를 탄다.  date_consume_ajax 파일에서 반환하는 값은 JSON 과
                // 클라이언트 사이트에서 스크립트로 실행할 코드가 전부이므로 IIFE 패턴으로 즉시실행한다.
                // +추가 => date.consume_ajax 파일의 ajax 호출 후 반환받는 값이 JSON 으로 파싱할수 없을경우 에러를 캐치하고있는데,
                // 파싱할수 없는 경우 alert를 띄우고자 의도했으므로 조건문을 제거함.
                // 브라우저별로 error 객체의 각 프로퍼티의 값이 다른 문제도 있음. (IE는 한글, 그외는 영어)
                new Function(result.responseText)();
              }
            })
            .catch(error => {
              alert(error);
            });
        },
        'js-approve-button--cancel': () => {
          // 취소하기 버튼
          self.close();
        },
        'js-close-complaint': () => {
          // 신고하기 창 닫기
          DOC.getElementById('complaint-layer').classList.remove('complaint-layer--activated');
        },
        'js-activate-user-panel': () => {
          // 채팅내역중 상대방 아이디 클릭으로 드롭다운메뉴 활성화
          activateUserPanel(event);
        },
        'js-background-holder': () => {
          // 상대방 드롭다운 메뉴 이외의 영역 클릭으로 드롭다운 메뉴 비활성화
          DOC.getElementById('js-background-holder').classList.remove(
            'background-holder--activated'
          );
        },
        'js-userPanel-user-info': () => {
          // 드롭다운 메뉴중 회원정보 클릭
          openWindowInScreenCenter({
            url: `/user/namecard.php?mb_id=${PRIVATE_CHAT.targetUserId}`,
            name: 'win_namecard',
            width: 755,
            height: 420
          });
        },
        'js-userPanel-send-msg': () => {
          // 드롭다운 메뉴중 쪽지보내기 클릭
          openWindowInScreenCenter({
            url: `/memo/memo_form.php?me_recv_mb_id=${PRIVATE_CHAT.targetUserId}`,
            name: '_memo',
            width: 613,
            height: 560
          });
        },
        'js-userPanel-add-friend': () => {
          // 드롭다운 메뉴중 친구추가 클릭
          openWindowInScreenCenter({
            url: `../pop/add_friend.php?mb_id=${PRIVATE_CHAT.targetUserId}`,
            name: `friend_${PRIVATE_CHAT.targetUserId}`,
            width: 433,
            height: 507
          });
        }
      };
    })();

    if (eventPack[findJSClassName] !== undefined) {
      eventPack[findJSClassName]();
    }
  });

  WIN.addEventListener('blur', () => {
    // 채팅창이 포커스를 잃었을때를 사용자가 '다른일을 하고있음' 으로 판단후 시스템 메세지를 추가한다.
    if (PRIVATE_CHAT.isAbleChat) {
      adjustNotifyLine('add', '여기까지 읽었습니다.');
    }
  });

  WIN.addEventListener('focus', () => {
    // 채팅창이 포커스를 다시 얻었을때 마지막 시스템메세지가 찍힌 스크롤탑으로 이동한다
    if (PRIVATE_CHAT.isAbleChat) {
      const lastNotifyLineElm = DOC.querySelectorAll('[data-title="여기까지 읽었습니다."]');

      if (lastNotifyLineElm.length) {
        lastNotifyLineElm[lastNotifyLineElm.length - 1].parentElement.scrollIntoView();
      }
    }
  });

  DOC.querySelector('#chat-input-text').addEventListener('keydown', event => {
    if (event.which === 13) {
      if (!event.shiftKey) {
        // 쉬프트 키와 함께 누른 엔터는 단순 줄바꿈 기능으로 둔다
        event.preventDefault();
        sendMessage(event);
      }
    } else {
      if (PRIVATE_CHAT.isAbleChat) {
        if (PRIVATE_CHAT.isTyping) {
          // 첫타이핑 이후 모든 타이핑 플래그 조건문은 이쪽으로 빠지며
          // 서버와 주고받는 패킷 데이터의 양을 줄이기 위해 함수 감속 패턴으로 매 타이핑 마다 setTypingFlag 함수를 1초씩 지연시킨다
          // 두번째 TYPING 패킷이 마지막 타이핑 이후 1초후 수신되는 순간 타이핑 락을 풀어 다시 첫 타자가 else 문으로 빠지도록 한다
          delayFunc(setTypingFlag, 1000);
          delayFunc(clearTyping, 1000, 'lock');
        } else {
          // 타이핑 플래그가 비활성화 되어있는 상태에서 첫 타이핑 시작시 최초 1회 동작
          PRIVATE_CHAT.isTyping = true;
          setTypingFlag();
        }
      }
    }
  });
});

function switchView(destination, resize) {
  // 화면을 전환시켜줌
  if (destination === 'chat') {
    DOC.getElementById('chatting-app').classList.add('chatting-app--activated');
    DOC.getElementById('ask-card-background').classList.remove('ask-card-background--activated');
    DOC.getElementById('ask-card-information').classList.remove('ask-card-information--activated');
  } else if (destination === 'card') {
    DOC.getElementById('chatting-app').classList.remove('chatting-app--activated');
    DOC.getElementById('ask-card-background').classList.add('ask-card-background--activated');
    DOC.getElementById('ask-card-information').classList.add('ask-card-information--activated');
  }

  if (resize) {
    WIN.resizeTo(462, WIN.outerHeight - WIN.innerHeight + DOC.body.offsetHeight); // 브라우저별 UI에 맞는 크기로 윈도우 리사이징
  }
}

function promiseAjax(method, url, params) {
  // Ajax 통신 을 위한 Promise를 반환하는 함수
  return new Promise((resolve, reject) => {
    const XMLHttp = new XMLHttpRequest();
    let convertedData = '';

    for (let i in params) {
      // key=value&key=value 의 형태로 파라미터를 변환
      if (params.hasOwnProperty(i)) {
        convertedData += `&${i}=${params[i]}`;
      }
    }

    if (method === 'POST') {
      XMLHttp.open(method, url, true);
    } else if (method === 'GET') {
      XMLHttp.open(method, url + params, true);
    }

    XMLHttp.onload = () => resolve(XMLHttp);
    XMLHttp.onerror = () => reject(error);

    if (method === 'POST') {
      XMLHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      XMLHttp.send(convertedData.slice(1));
    } else if (method === 'GET') {
      XMLHttp.send();
    }
  });
}

function activateUserPanel(event) {
  // 상대방 이름 클릭시 나타나는 유저 정보 패널
  const panelElm = DOC.getElementById('user-panel');

  DOC.getElementById('js-background-holder').classList.add('background-holder--activated');
  panelElm.setAttribute('style', `left: ${event.clientX}px; top: ${event.clientY}px;`);
}

function delayFunc(func, time, params) {
  // 함수 감속 패턴
  clearTimeout(func.tId);

  func.tId = setTimeout(() => {
    func(params);
  }, time);
}

function openWindowInScreenCenter(newWindowData) {
  // 새로운 윈도우를 화면의 중앙에 띄우기 위한 함수
  const openedNewWindowReference = window.open(
    newWindowData.url,
    newWindowData.name,
    `height=${newWindowData.height}, width=${newWindowData.width}, top=${(screen.height -
      newWindowData.height) /
      2}, left=${(screen.width - newWindowData.width) / 2}, scrollbars=0, resizable=0`
  );

  DOC.getElementById('js-background-holder').classList.remove('background-holder--activated'); //  상대방 드롭다운메뉴 비활성화

  if (openedNewWindowReference !== null) {
    openedNewWindowReference.focus();
  } else {
    alert('차단된 팝업창을 허용해 주십시오.');
  }
}

// 이하 채팅 소켓 스크립트
function connectSocket() {
  try {
    PRIVATE_CHAT.webSocket = new WebSocket(
      `ws://${PRIVATE_CHAT.serverHost}:${PRIVATE_CHAT.serverPort}/chat`
    );
    PRIVATE_CHAT.webSocket.onopen = () => {
      loginUser();
    };

    PRIVATE_CHAT.webSocket.onmessage = event => {
      // 해당 함수에서 패킷 데이터에 맞는 이벤트를 처리함
      processData(event.data);
    };

    PRIVATE_CHAT.webSocket.onclose = () => {
      if (PRIVATE_CHAT.isLogin) {
        // 로그인 상태라면 연결을 끊음
        PRIVATE_CHAT.webSocket.close();
        PRIVATE_CHAT.isAbleChat = false;
      }
    };
  } catch (error) {
    console.warn(`Error in connectSocket function [${error}]`);
  }
}

function disconnectSocket(errorMessage, dontClose) {
  // 채팅창 종료 로직
  if (errorMessage !== undefined && errorMessage.length > 0) {
    alert(errorMessage);
  }

  clearTyping('flag'); // 방이 폭파된후 여전히 타이핑 플래그가 활성화 돼있는 현상을 막기위해 제거함
  sendToDate({ cmd: 'END_DATE' }); // 채팅을 종료
  PRIVATE_CHAT.webSocket.close();

  if (!dontClose) {
    window.addEventListener('focus', () => {
      self.close(); // 시스템 메세지를 확인후 '확인' 버튼을 눌러 다시 채팅창이 포커스를 얻을시 채팅창을 종료
    });
  }
}

function loginUser() {
  PRIVATE_CHAT.webSocket.send(
    JSON.stringify({
      header: {
        version: '1.0',
        type: 'LOGIN',
        direction: 'server'
      },
      body: {
        cmd: 'LOGIN_DATE_USER',
        site_id: PRIVATE_CHAT.siteId,
        userid: PRIVATE_CHAT.userId,
        username: PRIVATE_CHAT.userInfo.name,
        session_id: PRIVATE_CHAT.sessionId,
        face_icon: '',
        age: PRIVATE_CHAT.age,
        gender: PRIVATE_CHAT.gender,
        area: PRIVATE_CHAT.area,
        level: PRIVATE_CHAT.level,
        key: PRIVATE_CHAT.key,
        user_status: '',
        mode: 'DATE',
        description: '',
        target_site_id: PRIVATE_CHAT.targetSiteId,
        target_userid: PRIVATE_CHAT.targetUserId,
        date_mode: PRIVATE_CHAT.dateMode,
        connect_mode: PRIVATE_CHAT.connectMode,
        model: '',
        uuid: '',
        version: '',
        browser_type: ''
      }
    })
  );
}

function addToChatList(sendId, sendName, message, fixScroll) {
  // 수신받은 데이터를 화면에 출력해주는 함수
  message = message.replace(
    /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi,
    '<a href="$1" target="_blank">$1</a>'
  );

  DOC.getElementById('chat-record-list').insertAdjacentHTML(
    'beforeend',
    `
            <li class="chat-record__item chat-record__item--${
              sendId === PRIVATE_CHAT.userId ? 'me' : 'you'
            }">
                <div data-user-id="${sendName}" class="chat-record__user ${
      sendId === PRIVATE_CHAT.userId ? '' : 'js-activate-user-panel'
    }">${sendName}</div>
                <span class="chat-record__text">${message}</span>
            </li>
        `
  );

  adjustScrollbar(fixScroll); // 내가 입력한 메세지가 출력될때는 스크롤 고정의 의도와는 상관없이 무조건 자동 스크롤 기능을 쓴다.
}

function adjustScrollbar(fixScroll) {
  // 자동 스크롤 기능 함수
  const chatListElm = DOC.getElementById('chat-record-list');
  const lastAddedListElm = chatListElm.lastElementChild;

  if (
    fixScroll ||
    chatListElm.scrollHeight - (chatListElm.scrollTop + chatListElm.offsetHeight) <=
      lastAddedListElm.offsetHeight
  ) {
    // fixScroll 이 true로 전달된다면 무조건 자동 스크롤 기능을 사용하며
    // 마지막으로 입력된 채팅 메세지의 높이와 스크롤이 안된 스크롤 높이가 같거나 작다면
    // 마지막 채팅이 추가되기전 자동 스크롤을 사용하고 있었던 것이므로
    // 사용자가 자동 스크롤을 원한다는 가정하에 스크롤을 내려준다
    lastAddedListElm.scrollIntoView();
  }
}

function setTypingFlag() {
  // 타이핑 플래그 설정을 위해 서버로 패킷을 쏘는 함수
  sendToDate({
    cmd: 'TYPING',
    userid: PRIVATE_CHAT.userId,
    username: PRIVATE_CHAT.userInfo.name
  });
}

function clearTyping(target) {
  // 타이핑 플래그 비활성화와 타이핑락을 풀어주기 위한 조건 함수
  if (target === 'flag') {
    DOC.getElementById('typing-sign').classList.remove('typing-sign--activated');
  } else if (target === 'lock') {
    PRIVATE_CHAT.isTyping = false;
  }
}

function sendMessage(event) {
  // 채팅 메세지를 서버로 전송하기 위한 함수
  if (PRIVATE_CHAT.dateMode === 'REQ_DATE' && !PRIVATE_CHAT.isAbleChat) {
    return false;
  }

  const eventTarget = event.type === 'click' ? event.target.previousElementSibling : event.target;
  const message = eventTarget.value.trim();

  if (message.length > 120) {
    adjustNotifyLine('add', '채팅 최대 글자를 초과하였습니다.');
  } else if (message.length > 0) {
    sendToDate({
      cmd: 'MSG',
      face_icon: '',
      userid: PRIVATE_CHAT.userId,
      username: PRIVATE_CHAT.userInfo.name,
      msg: message,
      style: ''
    });
  }

  eventTarget.value = '';
  eventTarget.focus();
}

function adjustNotifyLine(method, message) {
  // 채팅창에 시스템 메세지를 출력함
  if (message === '여기까지 읽었습니다.') {
    const hasUserChatLeastOne = DOC.querySelector('#chat-record-list [data-user-id]') !== null;
    const lastAddedNotifyElm = DOC.querySelector('#chat-record-list > li:last-child > hr');

    if (
      !hasUserChatLeastOne ||
      (!!lastAddedNotifyElm && lastAddedNotifyElm.dataset.title === '여기까지 읽었습니다.')
    ) {
      // 양 사용자 모두 채팅 입력을 1회조차 하지않았다면 추가하지 않음
      // 마지막 요소가 시스템 메세지 이며 그 메세지가 '여기까지 읽었습니다' 라면 추가하지 않음
      return false;
    } else {
      const duplicatedNotifyAsUnreadMSG = DOC.querySelector('[data-title="여기까지 읽었습니다."]');

      if (!!duplicatedNotifyAsUnreadMSG) {
        // 새로운 '여기까지 읽었습니다' 메세지를 추가하기위해 기존 시스템 메세지를 제거함
        duplicatedNotifyAsUnreadMSG.outerHTML = '';
      }
    }
  }

  if (method === 'add') {
    DOC.getElementById('chat-record-list').insertAdjacentHTML(
      'beforeend',
      `
                <li class="chat-record__item">
                    <hr data-title="${message}" class="chat-record__dotted-line js-chat-notify-line">
                </li>`
    );
    adjustScrollbar(true);
  } else if (method === 'replace') {
    const allNotifyLineElm = DOC.querySelectorAll('.js-chat-notify-line');

    allNotifyLineElm[allNotifyLineElm.length - 1].setAttribute('data-title', message);
  }

  if (message === '대화를 시작합니다.') {
    DOC.querySelector('.chat-record__item > hr').style.cssText =
      'color: #f95858; font-weight: bolder; border-color: #f95858';
  }
}

function rejectedAction() {
  // 상대방이 거절했을때 화면전환과 재 신청 대기를 위한 함수
  disconnectSocket(undefined, true); // 에러메세지 출력 없이 팝업창을 닫지 않은채 계속 진행
  PRIVATE_CHAT.isLogin = false; // 초기화가 필요한 속성 두가지를 초기화 함
  switchView('card', true); // 화면전환

  DOC.getElementById('approve-button--ok').classList.remove('button--activated');
  DOC.getElementById('approve-button--ok').setAttribute('disabled', 'true');
  DOC.getElementById('approve-button--cancel').classList.add('button--activated');
  DOC.getElementById(
    'private-chat-header'
  ).innerHTML = `상대방이 1:1 채팅을 거절했습니다.<br><span id="reconnectable-countdown">01:00</span> 이후 다시 신청 가능합니다.`;

  fiveMinuteInterval();
}

function fiveMinuteInterval() {
  // 상대방에게 거절당했을때 다시 신청 가능한 시간을 알려주기위해 1분간 카운트 다운함. 재귀함수
  const getCountdownElm = DOC.getElementById('reconnectable-countdown');
  let minute = parseInt(getCountdownElm.textContent.split(':')[0]);
  let second = parseInt(getCountdownElm.textContent.split(':')[1]);

  if (second - 1 >= 0) {
    second -= 1;
  } else {
    if (minute - 1 < 0) {
      resetSystemMessage();

      return false;
    }
    minute -= 1;
    second = 59;
  }

  getCountdownElm.textContent = `${(minute + '').length === 1 ? '0' + minute : minute}:${
    (second + '').length === 1 ? '0' + second : second
  }`;
  setTimeout(fiveMinuteInterval, 1000);
}

function resetSystemMessage() {
  // 채팅 신청 -> 거절 -> 1분의 시간이 흐른 후 재 신청이 가능함을 나타내는 시스템 메세지를 조건에 맞게 표출함
  const requestChatSystemMessage = [
    `<span class="font--color_orange">500</span>포인트를 소모하여 ${
      PRIVATE_CHAT.targetUserInfo.name
    } 회원에게 1:1 채팅을 신청합니다.<br>상대방이 채팅을 수락하면 <span class="font--color_orange">1,000</span>포인트가 추가로 소모됩니다.`,
    `${
      PRIVATE_CHAT.targetUserInfo.name
    } 회원에게 1:1 채팅을 신청합니다.<br>바로채팅 아이템을 사용 중입니다.`,
    `${
      PRIVATE_CHAT.targetUserInfo.name
    } 회원에게 1:1 채팅을 신청합니다.<br>상대방이 채팅을 수락하면 <span class="font--color_orange">2,000</span>포인트가 소모됩니다.`
  ];
  let systemMessageIndex;

  if (PRIVATE_CHAT.targetUserInfo.name.indexOf('손님_') > -1) {
    // 손님일때
    systemMessageIndex = 0;
  } else {
    // 회원일때
    if (PRIVATE_CHAT.isFreeChat === 'true') {
      // 바로채팅 아이템 소유중일때
      systemMessageIndex = 1;
    } else {
      // 바로채팅 아이템 미소유중일때
      systemMessageIndex = 2;
    }
  }

  DOC.getElementById('private-chat-header').innerHTML =
    requestChatSystemMessage[systemMessageIndex];

  if (DOC.getElementById('approve-button--ok').hasAttribute('disabled')) {
    DOC.getElementById('approve-button--ok').removeAttribute('disabled');
    DOC.getElementById('approve-button--ok').classList.add('button--activated');
  }

  WIN.resizeTo(462, WIN.outerHeight - WIN.innerHeight + DOC.body.offsetHeight); // 브라우저별 UI에 맞는 크기로 윈도우 리사이징
}

function sendToDate(data) {
  // 서버로 데이터를 쏨
  try {
    PRIVATE_CHAT.webSocket.send(
      JSON.stringify({
        header: {
          version: '1.0',
          type: 'DATE',
          direction: 'server'
        },
        body: data
      })
    );
  } catch (e) {
    console.warn(`Error in sendToDate function [${error}]`);
  }
}

function processData(data) {
  // 서버로부터 받은 패킷 데이터 처리
  const packet = JSON.parse(data);
  const headerPacketType = packet.header.type;

  if (headerPacketType === 'LOGIN') {
    processLogin(packet.body);
  } else if (headerPacketType === 'DATE') {
    processDate(packet.body);
  }
}

function processLogin(data) {
  // LOGIN 패킷 데이터를 쏘아올린후 수신받은 응답에 대한 처리를 함
  try {
    if (data.cmd === 'LOGIN_SUCCESS') {
      if (!PRIVATE_CHAT.isLogin) {
        PRIVATE_CHAT.isLogin = true;

        if (PRIVATE_CHAT.dateMode === 'ACT_DATE') {
          sendToDate({
            cmd: 'ACT_DATE',
            target_userid: PRIVATE_CHAT.targetUserId,
            target_site_id: PRIVATE_CHAT.targetSiteId
          });
        } else if (PRIVATE_CHAT.dateMode === 'REQ_DATE') {
          sendToDate({
            cmd: 'REQ_DATE',
            site_id: PRIVATE_CHAT.siteId,
            target_userid: PRIVATE_CHAT.targetUserId,
            target_site_id: PRIVATE_CHAT.targetSiteId
          });
        }
      } else {
        alert('비정상적인 로그인 입니다. 채팅창을 종료합니다.');
        disconnectSocket();
      }
    } else if (data.cmd === 'ERROR') {
      const errorEventPack = (() => {
        return {
          NOT_EXISTS_USER: () => {
            alert('이미 1:1 채팅이 종료된 방입니다.');
          },
          ALREADY_END: () => {
            alert('상대방이 퇴장하였습니다');
          },
          DISCONNECTED: () => {
            alert('연결이 종료되었습니다.');
          },
          REQUEST_MAX_EXCEED: () => {
            alert('짧은시간에 연속하여 채팅 신청을 보낼수 없습니다.');
          },
          ALREADY_REQUEST: () => {
            alert('짧은시간에 연속하여 채팅 신청을 보낼수 없습니다.');
          }
        };
      })();

      if (errorEventPack[data.type] !== undefined) {
        errorEventPack[data.type]();
        disconnectSocket();
      }
    }
  } catch (e) {
    console.warn(`Error in processLogin function [${error}]`);
  }
}

function processDate(data) {
  // DATE 패킷 데이터를 처리함
  const processDateEventPack = (() => {
    return {
      TYPING: () => {
        // MSG 패킷을 먼저 받아 타이핑 플래그가 초기화 상태라면 해당 속성만 기본값으로 돌려주고 즉시 빠져나간다
        if (processDate.isAlreadyClearTyping) {
          processDate.isAlreadyClearTyping = false;

          return;
        }
        // 상대방이 입력중임을 나타내는 패킷을 받은 후 타이핑 플래그 처리 (나의 채팅중을 내게 표시하지 않음)
        // 타이핑 패킷 데이터를 서버로 올리는 쪽에서 타이핑 시작시 한번, (타이핑이 끝난후 + 1초후) 한번
        // 총 두번의 패킷을 쏘아올리므로 메세지를 받으려는 쪽에서도 총 두번의 TYPING 패킷이 떨어진다
        // 이때 타이핑플래그의 상태에 따라 활성화 여부를 결정하는데
        // 정상이라면 첫 패킷은 플래그를 활성화 시킬것이고 두번째 패킷은 플래그를 비활성화 시킬것이다
        if (data.userid !== PRIVATE_CHAT.userId) {
          const typingElm = DOC.getElementById('typing-sign');

          // 활성화 상태라면
          if (typingElm.classList.contains('typing-sign--activated')) {
            // 함수감속패턴으로 인해 타이핑이 끝난후 1초후 TYPING 패킷이 쏘아 올려지므로
            // 받는쪽에서 +2 초를 더 지연시켜 총 타이핑이 끝난후 약 3초후 플래그를 비활성화 한다
            // 타이핑 플래그 비활성화 시기를 앞당기고싶다면 여기서 조절하는것이 좋다
            delayFunc(clearTyping, 2000, 'flag');
          } else {
            typingElm.classList.add('typing-sign--activated');
          }
        }
      },
      MSG: () => {
        // 서버로부터 받은 메세지를 출력하는 함수로 전달함
        const len = data.messages.length;
        let i = 0;

        for (; i < len; i++) {
          const thisMessage = data.messages[i];

          if (PRIVATE_CHAT.userId === thisMessage.userid) {
            addToChatList(thisMessage.userid, thisMessage.username, thisMessage.msg, true);
          } else {
            addToChatList(thisMessage.userid, thisMessage.username, thisMessage.msg, false);
          }
        }

        // 첫 TYPING 패킷이 내려와 타이핑 플래그를 활성화 시켜 놓은 상태에서 두번째 패킷이 떨어지기전에
        // 채팅 입력으로 MSG가 먼저 떨어졌다면 TYPING 에서 초기화 시키기 전에 MSG 에서 이미 초기화 되었음을 기록한다
        processDate.isAlreadyClearTyping = true;
        clearTyping('flag'); // 메세지가 떨어지면 채팅 입력 플래그를 제거하고
        clearTyping('lock'); // 타이핑 락을 함께 풀어준다
        clearTimeout(processDate.divingTId); // 메세지 입력이 있다면 잠수 타이머를 제거하고
        processDate.divingTId = delayFunc(
          disconnectSocket,
          600000,
          '10분간 입력이 없어 채팅창을 종료합니다.'
        ); // 잠수 타이머를 10분후로 다시 설정해 10분간 입력이 없을시 창을 닫아주는 메서드를 지연시킨다
      },
      START: () => {
        // 신청을 받은 사람이 채팅 수락 버튼을 누르고 두사람이 모두 채팅창에 입장했을때 받는 패킷
        if (PRIVATE_CHAT.dateMode === 'REQ_DATE') {
          promiseAjax('POST', './ajax.common.php', {
            cmd: 'date_user',
            target_userid: PRIVATE_CHAT.targetUserId
          })
            .then(result => {
              const data = JSON.parse(result.responseText);

              if (data.returnCode === '100') {
                PRIVATE_CHAT.isAbleChat = true;
                adjustNotifyLine('replace', '대화를 시작합니다.');
              } else {
                alert(data.errorMsg);
              }
            })
            .catch(error => {
              alert(error);
            });
        } else if (PRIVATE_CHAT.dateMode === 'ACT_DATE') {
          PRIVATE_CHAT.isAbleChat = true;
          adjustNotifyLine('add', '대화를 시작합니다.');
        }
        setTimeout(sendPing, 30000); // 30초 마다 ping 전송
      },
      RCT_DATE: () => {
        // 로비나 공개채팅에서 상대방이 초대를 거부했을때 떨어지는 패킷
        rejectedAction();
      },
      EXIT_DATE_USER: () => {
        adjustNotifyLine('add', '상대방이 퇴장하였습니다.');
        disconnectSocket(undefined, true);
      },
      ERROR: () => {
        if (data.type === 'TARGET_NOT_EXISTS') {
          alert(`${PRIVATE_CHAT.targetUserInfo.name} 님은 채팅중이 아닙니다.`);
        } else if (data.type === 'ALREADY_REQUEST') {
          alert('짧은시간에 연속하여 채팅 신청을 보낼수 없습니다.');
        }
      }
    };
  })();

  if (processDateEventPack[data.cmd] !== undefined) {
    processDateEventPack[data.cmd]();

    if (data.cmd === 'ERROR') {
      disconnectSocket();
    }
  }
}

function sendPing() {
  sendToDate({ cmd: 'PING' });
  setTimeout(sendPing, 30000);
}

var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var postcssImport = require("postcss-import");

gulp.task('default', function () {
    var plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        cssnano({
            'postcss-minify-font-values': false
        }),
        postcssImport()
    ];
    return gulp.src('src/private-chat.css')
        .pipe(postcss(plugins))
        .pipe(gulp.dest('./dist/css'));
});